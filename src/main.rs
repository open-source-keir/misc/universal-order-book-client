pub mod order_book_summary {
    tonic::include_proto!("order_book_summary");
}
use order_book_summary::order_book_aggregator_client::OrderBookAggregatorClient;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut client = OrderBookAggregatorClient::connect("http://127.0.0.1:50052").await
        .expect("Unable to connect");

    let request = tonic::Request::new(order_book_summary::Empty{});

    let mut stream = client
        .book_summary(request)
        .await
        .expect("cannot book summary")
        .into_inner();


    while let Some(summary) = stream.message().await? {
        println!("Got response: {:?}", summary);
    }

    Ok(())
}
